#include <stdio.h>
#include "dirent.h"
#include "filelist.h"


size_t file_list(const char *path, char ***ls) {
	size_t count = 0;
	size_t length = 0;
	DIR *dp = NULL;
	struct dirent *ep = NULL;

	dp = opendir(path);
	if (NULL == dp) {
		fprintf(stderr, "no such directory: '%s'", path);
		return 0;
	}

	*ls = NULL;
	ep = readdir(dp);
	while (NULL != ep) {
		count++;
		ep = readdir(dp);
	}

	rewinddir(dp);
	*ls = calloc(count, sizeof(char *));

	count = 0;
	ep = readdir(dp);
	char *ext;
	while (NULL != ep) {
		ext = get_filename_ext(&(ep->d_name));
		if (!strcmp(ext, ".weights")) {
			(*ls)[count++] = strdup(ep->d_name);
		}
		ep = readdir(dp);
	}

	closedir(dp);
	return count;
}

int main(int argc, char **argv) {

	char **files;
	size_t count;
	int i;

	count = file_list(argv[1], &files);
	char *fullpath = concat(argv[1], "filelist.csv");
	FILE *f = fopen(fullpath, "w");
	fprintf(f, "ModelFile, IoU, Recall\n");

	for (i = 0; i < count; i++) {
		fprintf(f, "%s%s\n", argv[1], files[i]);
		files[i] = concat(argv[1], files[i]);
		printf("%s\n", files[i]);

	}
	printf("Total number of files is %d\n", count);
	
	printf("Saved csv file with these filenames to: \n%s\n", fullpath);
	fclose(f);
}