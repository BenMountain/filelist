# README #

The filelist program searches a directory for files that match a certain file extension.

It can accept inputs from other C programs and pass back a variable list and/or can run as a stand alone that saves the list of files the user wants to a .csv.

### How do I get set up? ###

Dependencies required are already included in the source code. 

dirent.h is an open-source header obtained through a github repository. dirent.h is native to Linux and this program uses the openly distributed Windows version developed by 3rd parties.


### Who do I talk to? ###

To run pass two arguments to the program.

Argument 1: Folder location to be scanned. For example: C:/users/Jim/Documents

Argument 2: File extension you are looking for. For example: .mp4

Full example input:  ./filelist.exe C:/users/Jim/Documents .mp4

The program will scan that directory(it does not scan sub-folders) and outputs a .csv file saved to that directory with the desired filelist.
If the file extension you are looking for is .csv, it the saved output file will not be part of that input since it is created after the folder scan occurs.